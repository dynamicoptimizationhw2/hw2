function [x_dot] = atrius( t, x, K)
    global A B xd

    x_dot = A * x - B * K * (x - xd');
end