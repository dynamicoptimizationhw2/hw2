%% Backwards pass function
function [ du, Vx ] = backward_pass( x, u, N, A, B, H, R, Vx )
% backward pass of trajectory optimization

x = x';
Lxx = H;
Luu = R;
Lxu = zeros(size(Lxx));

for i = N:-1:1
 Lx = H*x(:,i);
 Lu = R*u(:,i);
 Qx = Lx + A'*Vx(i+1);
 Qu = Lu + B'*Vx(i+1);
 c = 0;
 for j = i:N
     c = c + 1;
 end
 Qxx = Lxx + c*A'*H*A;
 Qux = c*B'*H*A;
 Quu = Luu + c*B'*H*B;
 du(:,i) = Quu\Qux;
 k = Quu\Qu;
 Vx(i) = Qx' - Qu*k;
 Vxx(i) = Qxx - Qux*k;
end