function [score] = fit_fxn( in )
    global A B Q R K xd 
    
    in;
    Q_local = diag(in(1:6));
    R_local = diag(in(7:8));
    K_local = lqr(A, B, Q_local, R_local);
    
    x_0 = [-1, 1.1, 0, 1, 0, 0];
    
    tf = 1; % s
    T = 1e-4; % s
    t_edges = 0:T:tf; % s
    
    [t, x] = ode45(@(t,x) atrius(t,x,K_local), t_edges, x_0);
        
    score = (x(end,1) - xd(:,1))^2 + (x(end,4) - xd(:,4))^2;
    
end