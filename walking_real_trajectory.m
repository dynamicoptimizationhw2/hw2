%% Information
% Generate the 10m steps
clear
clc
%% Variables
global A_1 B_1 A_2 B_2 A_3 B_3 A B Q R K xd 
action_parameterization
xd = [0.5, 1.1, 0, 1, 0, 0];
choice = 2;

if choice == 1
    A = A_1;
    B = B_1;
    load('qr_walking_1.mat');
    load('X1_trajectory.mat');
end
if choice == 2
    A = A_2;
    B = B_2;
    load('qr_walking_2.mat');
    load('X2_trajectory.mat');
end
if choice == 3
    A = A_3;
    B = B_3;
    load('qr_walking_3.mat');
    load('X3_trajectory.mat');
end
K = lqr(A, B, Q, R);
x_0 = zeros(1,6);
X = [0, 1.1, 0, 0, 0, 0];
tf = 0.5; % s
T = 1e-4; % s
t_edges = 0:T:tf; % s
action_parameterization
%% Action Parameterization
for i = 1:0.5:10
    if i == 10
        xd = [0, 1.1, 0, 0, 0, 0];
    end
    x_0(1,1) = - x_traj(i*2-1,1) + X(end,1);
    x_0(1,2:end) = X(end, 2:end);
    [t, x] = ode45(@(t,x) atrius(t,x,K), t_edges, x_0);
    disp('-----')
    disp('Initial X (foot)')
    x_0(1)
    disp('Initial X (global)')
    X(end,1)
    disp('Final X (foot)')
    x(end,1)
    disp('Final Vx (foot)')
    x(end,4)
    x(:,1) = x(:,1) + X(end,1) - x(1,1);
    X = [X; x];
    x(:,1) = x(:,1) + X(end,1);
    disp('Final X (global)')
    X(end,1)
end

% Plot the steps
stair = [0,0];
for i = 1:1:10
    stair = [stair; [i, i-1]];
    stair = [stair; [i, i]];
end

X(:,1) = X(:,1);
bigT = linspace(0, 10, size(X,1));
hold on;
plot(bigT, X(:,1));
plot(stair(:,1), stair(:,2), 'r');
hold off;