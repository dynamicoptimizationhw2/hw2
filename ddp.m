%% Information
% Generate trajectory using DDP
clear;
clc;
%% Variables
global A_1 B_1 A_2 B_2 A_3 B_3 A B Q R K xd 
action_parameterization
xd = [0.5, 1.1, 0, 1, 0, 0];
choice = 2;

if choice == 1
    A = A_1;
    B = B_1;
    load('qr_walking_1.mat');
    load('X1_trajectory.mat');
    load('X_1.mat')
    
end
if choice == 2
    A = A_2;
    B = B_2;
    load('qr_walking_2.mat');
    load('X2_trajectory.mat');
    load('X_2.mat')

end
if choice == 3
    A = A_3;
    B = B_3;
    load('qr_walking_3.mat');
    load('X3_trajectory.mat');
    load('X_3.mat')
end
K = lqr(A, B, Q, R);
x_0 = zeros(1,6);
tf = 0.5; % s
T = 1e-4; % s
t_edges = 0:T:tf; % s
action_parameterization
N = size(X,1);
u = zeros(2,N);
xdes = [0.5,1.1,0,0,0,0];
Vx = zeros(1,N);
for i = 1:N
    if (rem(i,5001) == 0)
        xdes(1) = xdes(1) + 0.5;
    end
    u(:,i) = -K*(X(i,:) - xdes)';
    for j = i:N
        xdes_local = xdes;
        if (rem(j,5001) == 0)
            xdes_local(1) = xdes_local(1) + 0.5;
        end      
        Vx(i) = Vx(i) + 0.5*(X(j,:) - xdes_local)*Q*(X(j,:) - xdes_local)' + 0.5*(-K*(X(j,:) - xdes_local)')'*R*(-K*(X(j,:) - xdes_local)');
    end
    i
end
%%
x0 = X(1,:)';

[ du, Vx ] = backward_pass(X, u, N, A, B, Q, R,Vx);
[ x, u, Vx] = forward_pass( x0, du, N, A, B, K, x)




