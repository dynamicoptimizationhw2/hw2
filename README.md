Homework 2 for Dynamic Optimization
* Cole, Pranav, Dan, Mohak

Part 1 (Action Parameterizations):
* Here we developed the A and B matrices for each of the three action parameterizations.
Code for Part 1:
* action_parameterization.m

Part 1 (LQR):
* Here we used fmincon and minimized distance in x_desired and x_dot_desired as our cost function
* We ran this for each of the action parameterizations in order to find the best Q and R values for each
Minimization function:
* fit_fxn.m
Dynamics function:
* atrius.m
Main function:
* LQR.m
Data for part 1:
* qr_1.mat
* qr_2.mat
* qr_3.mat
* qr_walking_1.mat
* qr_walking_2.mat
* qr_walking_3.mat

Part 2:
* Here we generated walking trajectories for the specified footstep pattern using each action parameterization
Dynamics:
* atrius.m
Main Function:
* walking.m
Data for part 2:
* X_1.mat
* X_2.mat
* X_3.mat

Part 3(a):
* Here we used the trajectories from Part 2 in order to use LQR on this COM trajectories for better control
Dynamics:
* atrius.m
Main Function:
* walking_real_trajectory.m
Data for Part 3(a):
* X1_trajectory.mat
* X2_trajectory.mat
* X3_trajectory.mat

Part 3(b):
* Here we implemented DDP on the robot's trajectory to further improve the solution
Dynamics:
* atrius.m
Main Function:
* ddp.m
Support Functions:
* backward_pass.m
* forward_pass.m
