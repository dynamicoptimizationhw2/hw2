function [c, ceq] = mycon(in)
    q = diag(in(1:6));
    r = diag(in(7:8));
    [~,p1] = chol(q);
    [~,p2] = chol(r);
    ceq(1,1) = p1;
    ceq(1,2) = p2;
    c = [];
end