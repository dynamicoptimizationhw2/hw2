function [ x, u, Vx] = forward_pass( x0, du, N, A, B, K, x)
% forward pass of trajectory optimization
% This returns N commands, and N+1 states
% x0 = initial state
% du = from backward
% N = number of commands N+1 = number of states
% A = linearized A matrix
% B = linearized B matrix
% k = K matrix from backward pass

x = x';
x0 = x0';
x_new(:,1) = x0;

% for i = 1:(N-1)
%  uu_new(i) = - K*(xx_new(1:3,i) - xx(1:3,i)) + du(i) + uu(i);
%  xx_new(4,i) = uu_new(i);
%  z = xx_new(1:3,i);
%  z(3) = z(3) + p_d(i);
%  xx_new(1:3,i+1) = A*z + B*uu_new(i);
% end
u_new = zeros(2,N-1);
for i = 1:(N-1)
    u_new(:,i) = u(:,i) - du(:,i) - K*(x_new(:,i) - x(:,i));
    x_new(:,i+1) = A*x_new(:,i) + B*u_new(:,i);
end

x = x_new;
u = u_new;
for i = 1:N
    for j = i:N
        Vx(i) = V(i) + 0.5*(x_new(:,j) - x(:,j));
    end
end

% ii = 1:1000;
% plot(ii,p_d(ii),'b',ii,xx_new(3,ii)+p_d(ii),'c',ii,xx_new(1,ii),'r')

