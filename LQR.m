%% Information
% This script is designed to run LQR on the action parameterizations in the
% script action_parameterization
%% =======================================================================
% Constants and varibles
%=========================================================================
% Global
clc
clear
global A_1 B_1 A_2 B_2 A_3 B_3 A B Q R K xd 
% Local
Q = [1, 1, 1, 1, 1, 1];
R = [1, 1];
in_0 = [Q, R];
lb = zeros(size(in_0));
xd = [0, 1.1, 0, 1, 0, 0];
action_parameterization
% %% =======================================================================
% % Run LQR on Action Parameterization 1
% %=========================================================================
%% Find the best Q and R for 1
A = A_1;
B = B_1;
in_final = fmincon(@fit_fxn, in_0, [], [], [], [], lb, [], []);
Q = diag(in_final(1:6))
R = diag(in_final(7:8))
save('qr_walking_1.mat', 'Q', 'R');
% %% Run dynamics
% x_0 = [-1, 1.1, 0, 1, 0, 0];
% A = A_1;
% B = B_1;
% Q = eye(6);
% R = eye(2);
% load('qr_walking_1.mat');
% Q(4,4) = 0;
% K = lqr(A, B, Q, R);
% tf = 1; % s
% T = 1e-4; % s
% t_edges = 0:T:tf; % s
% [t, x] = ode45(@(t,x) atrius(t,x,K), t_edges, x_0);
% figure;
% [X, H1, H2] = plotyy(t, x(:,1), t, x(:,4), 'plot');
% xlabel('Time(s)');
% set(get(X(1), 'Ylabel'), 'String', 'x');
% set(get(X(2),'Ylabel'), 'String', 'x_{dot}');
%% =======================================================================
% Run LQR on Action Parameterization 2
%=========================================================================
% Find the best Q and R for 2
A = A_2;
B = B_2;
in_final = fmincon(@fit_fxn, in_0, [], [], [], [], lb, [], []);
Q = diag(in_final(1:6))
R = diag(in_final(7:8))
save('qr_walking_2.mat', 'Q', 'R');
% % Run the dynamics
% x_0 = [0, 1.1, 0, 0.5, 0, 0.5];
% K = lqr(A_2, B_2, Q, R);
% tf = 20; % s
% T = 1e-4; % s
% t_edges = 0:T:tf; % s
% [t, x] = ode45(@(t,x) atrius(t,x,K), t_edges, x_0);
% figure;
% [X, H1, H2] = plotyy(t, x(:,1), t, x(:,2), 'plot');
% xlabel('Time(s)');
% set(get(X(1), 'Ylabel'), 'String', 'x');
% set(get(X(2),'Ylabel'), 'String', 'y');
% title('Plot of x and y');
%% =======================================================================
% Run LQR on Action Parameterization 3
%=========================================================================
A = A_3;
B = B_3;
Q = [1, 1, 1, 1, 1, 1];
R = [1, 1];
in_0 = [Q, R];
in_final = fmincon(@fit_fxn, in_0, [], [], [], [], lb, [], []);
Q = diag(in_final(1:6));
R = diag(in_final(7:8));
save('qr_walking_3.mat', 'Q', 'R');
% x_0 = [0, 1.1, 0, 0.5, 0, 0.5];
% K = lqr(A, B, Q, R);
% tf = 20; % s
% T = 1e-4; % s
% t_edges = 0:T:tf; % s
% [t, x] = ode45(@(t,x) atrius(t,x,K), t_edges, x_0);
% figure;
% [X, H1, H2] = plotyy(t, x(:,1), t, x(:,2), 'plot');
% xlabel('Time(s)');
% set(get(X(1), 'Ylabel'), 'String', 'x');
% set(get(X(2),'Ylabel'), 'String', 'y');